import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
  Logger,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { PrismaService } from '../prisma/prisma.service';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  constructor(private logger: Logger, private prisma: PrismaService) {}
  async catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();

    const statusCode =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;

    const errResponse: any = {
      path: request.url,
      method: request.method,
      errorName: exception?.name,
      message:
        exception?.message === 'Bad Request Exception'
          ? (exception.getResponse() as { message: [] }).message
          : exception?.message,
      statusCode,
      timestamp: new Date().toISOString(),
    };

    await this.prisma.logs.create({
      data: {
        routeMethod: request.route.stack[0].method,
        routePath: request.route.path,
        status: 'Failed',
      },
    });

    this.logger.log(
      `request method: ${request.method} request url${request.url}`,
      errResponse,
    );

    response.status(statusCode).json(errResponse);
  }
}
