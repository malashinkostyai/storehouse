import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

async function main() {
  const rackArr = ['AA', 'AB', 'BA', 'BB', 'CA', 'CB'];
  const sectionArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  for (const rack of rackArr) {
    const createdRack = await prisma.rack.create({
      data: {
        id: rack,
      },
    });
    console.log(createdRack);
  }

  for (let i = 0; i < rackArr.length; i++) {
    for (let y = 0; y < sectionArr.length; y++) {
      const createdSection = await prisma.section.create({
        data: {
          rackId: rackArr[i],
          sectionNumber: sectionArr[y],
        },
      });
      console.log(createdSection);
    }
  }
}
main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
