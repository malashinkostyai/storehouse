export class ExtractDataUtil {
  extractIdAndSize(productArr: { id: string }[]) {
    const newProductArr: { id: number; size: string }[] = [];

    for (const product of productArr) {
      const extractedId = parseInt(product.id.substring(1, 6));
      const extractedSize = product.id.substring(8);

      newProductArr.push({ id: extractedId, size: extractedSize });
    }

    return newProductArr;
  }
}
