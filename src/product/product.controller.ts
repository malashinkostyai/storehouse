import {
  Body,
  Controller,
  Delete,
  Get,
  Post,
  Query,
  UseInterceptors,
} from '@nestjs/common';
import { LocationBodyDto } from './dtos/location.dto';
import { ProductBodyDto } from './dtos/productBody.dto';
import { LoggingInterceptor } from './interceptors/log.interceptor';
import { ProductService } from './product.service';

@UseInterceptors(LoggingInterceptor)
@Controller('product')
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @Get('/getLocations')
  getLocations(@Query() { id, quantity }: LocationBodyDto) {
    return this.productService.getLocationsOfProduct(id, quantity);
  }

  @Post('/addProducts')
  createProducts(@Body() body: ProductBodyDto) {
    return this.productService.create(body);
  }

  @Delete('/deleteProducts')
  deleteProducts(@Body() body: ProductBodyDto) {
    return this.productService.remove(body);
  }
}
