import {
  BadRequestException,
  ConflictException,
  Injectable,
  Logger,
} from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { ProductBodyDto } from './dtos/productBody.dto';
import { ExtractDataUtil } from './util/extractData';

@Injectable()
export class ProductService {
  constructor(
    private prisma: PrismaService,
    private extractData: ExtractDataUtil,
    public logger: Logger,
  ) {}

  async create({ rack, sectionNumber, products }: ProductBodyDto) {
    const section = await this.prisma.section.findUnique({
      where: {
        sectionNumber_rackId: {
          rackId: rack,
          sectionNumber,
        },
      },
    });

    if (!section) {
      throw new BadRequestException(
        `There is no such location with rack = ${rack} and section = ${sectionNumber}`,
      );
    }

    const extractedProducts = this.extractData.extractIdAndSize(products);

    const insertedProducts: {
      productQuantity: number;
      product: {
        id: number;
        size: string;
      };
    }[] = [];

    for (const product of extractedProducts) {
      const existingProduct = await this.prisma.findById(product.id);

      if (!existingProduct) {
        const createdProduct = await this.prisma.product
          .create({
            data: { id: product.id, size: product.size },
          })
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          .catch((_err: any) => {
            throw new ConflictException(
              `ProductID with ID = ${product.id} is not greater than 10000 and lower than 50000`,
            );
          });

        this.logger.log('Created product', createdProduct);
      }

      const addedToSectionProduct = await this.prisma.sectionOnProduct.upsert({
        select: {
          product: {
            select: {
              id: true,
              size: true,
            },
          },
          productQuantity: true,
        },
        where: {
          sectionId_productId: {
            productId: product.id,
            sectionId: section.id,
          },
        },
        update: {
          productQuantity: {
            increment: 1,
          },
        },
        create: {
          sectionId: section.id,
          productId: product.id,
          productQuantity: 1,
        },
      });

      insertedProducts.push(addedToSectionProduct);

      this.logger.log('Inserted products', insertedProducts);
    }
    return insertedProducts;
  }

  async remove({ rack, sectionNumber, products }: ProductBodyDto) {
    const section = await this.prisma.section.findUnique({
      where: {
        sectionNumber_rackId: {
          rackId: rack,
          sectionNumber,
        },
      },
    });

    if (!section) {
      throw new BadRequestException(
        `There is no such location with rack = ${rack} and section = ${sectionNumber}`,
      );
    }

    const extractedProducts = this.extractData.extractIdAndSize(products);

    const existingProducts = [{ updated: [] }, { deleted: [] }];

    for (const product of extractedProducts) {
      const sectionOnProduct = await this.prisma.sectionOnProduct.findFirst({
        where: {
          productId: product.id,
          sectionId: section.id,
        },
      });

      if (!sectionOnProduct) {
        continue;
      }

      if (sectionOnProduct.productQuantity > 1) {
        const updatedProductQuantity =
          await this.prisma.sectionOnProduct.update({
            select: {
              productId: true,
              productQuantity: true,
            },
            where: {
              sectionId_productId: {
                productId: product.id,
                sectionId: section.id,
              },
            },
            data: {
              productQuantity: {
                decrement: 1,
              },
            },
          });

        existingProducts[0].updated.push(updatedProductQuantity);

        continue;
      }

      const deletedProduct = await this.prisma.product.delete({
        select: {
          id: true,
          size: true,
        },
        where: {
          id: product.id,
        },
      });

      existingProducts[1].deleted.push(deletedProduct);
    }

    this.logger.log(`Updated product's count`, existingProducts);

    return existingProducts;
  }

  async getLocationsOfProduct(id: number, quantity: number) {
    const [product] = await this.prisma.sectionOnProduct.groupBy({
      by: ['productId'],
      where: {
        productId: id,
      },
      _sum: {
        productQuantity: true,
      },
    });

    if (product?._sum.productQuantity < quantity) {
      throw new BadRequestException(
        `There is no such amount of products = ${quantity} in stock`,
      );
    }

    const locations = await this.prisma.sectionOnProduct.findMany({
      orderBy: [{ productQuantity: 'asc' }],
      where: {
        productId: id,
        productQuantity: {
          gte: 1,
        },
      },

      select: {
        productId: true,
        productQuantity: true,
        section: {
          select: {
            rackId: true,
            sectionNumber: true,
          },
        },
      },
    });

    if (locations.length === 0) {
      throw new BadRequestException(
        `There is no product with id = ${id} in stock`,
      );
    }

    const filteredLocations = [];
    let availableQuantity = 0;

    for (const location of locations) {
      if (availableQuantity < quantity) {
        availableQuantity += location.productQuantity;

        filteredLocations.push(location);
      }
    }

    this.logger.log('getLocationsOfProduct', filteredLocations);

    return filteredLocations;
  }
}
