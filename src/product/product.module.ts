import { Logger, Module } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';
import { HttpExceptionFilter } from '../error/http-exception.filter';
import { PrismaService } from '../prisma/prisma.service';
import { ProductController } from './product.controller';
import { ProductService } from './product.service';
import { ExtractDataUtil } from './util/extractData';

@Module({
  imports: [],
  controllers: [ProductController],
  providers: [
    Logger,
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter,
    },
    ProductService,
    PrismaService,
    ExtractDataUtil,
  ],
})
export class ProductModule {}
