import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { map } from 'rxjs';
import { PrismaService } from '../../prisma/prisma.service';

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  constructor(private prisma: PrismaService) {}

  async intercept(context: ExecutionContext, next: CallHandler): Promise<any> {
    const { route } = context.switchToHttp().getRequest();

    return next.handle().pipe(
      map(async (data: any) => {
        if (data.length > 0) {
          await this.prisma.logs.create({
            data: {
              routeMethod: route.stack[0].method,
              routePath: route.path,
              status: 'Completed',
            },
          });
        }
        return data;
      }),
    );
  }
}
