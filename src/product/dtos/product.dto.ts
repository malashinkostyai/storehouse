import { Matches } from 'class-validator';

export class Product {
  @Matches(
    '^L(1000[1-9]|100[1-9][0-9]|10[1-9][0-9]{2}|1[1-9][0-9]{3}|[2-4][0-9]{4})\\sS[A-Z0-9]?[A-Z]?[A-Z]$',
  )
  id: string;
}
