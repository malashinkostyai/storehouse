import { Transform } from 'class-transformer';
import { IsNumber, Max, Min } from 'class-validator';

export class LocationBodyDto {
  @Transform(({ value }) => parseInt(value))
  @IsNumber()
  @Min(10001)
  @Max(49999)
  id: number;

  @Transform(({ value }) => parseInt(value))
  @IsNumber()
  @Min(1)
  @Max(100)
  quantity: number;
}
