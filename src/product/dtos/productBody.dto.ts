import { Type } from 'class-transformer';
import {
  IsArray,
  IsIn,
  IsNumber,
  Max,
  Min,
  ValidateNested,
} from 'class-validator';
import { Product } from './product.dto';

export class ProductBodyDto {
  @IsIn(['AA', 'AB', 'BA', 'BB', 'CA', 'CB'])
  rack: string;

  @IsNumber()
  @Min(1)
  @Max(10)
  sectionNumber: number;

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => Product)
  products: Product[];
}
