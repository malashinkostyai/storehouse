import * as request from 'supertest';
import { HttpStatus, INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { ProductModule } from '../src/product/product.module';

describe('Products (e2e)', () => {
  let app: INestApplication;

  const productsBody = {
    rack: 'CB',
    sectionNumber: 1,
    products: [
      {
        id: 'L45555 S2X',
      },
      {
        id: 'L45554 S2XL',
      },
    ],
  };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [ProductModule],
    }).compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  describe('POST /products/addProducts', () => {
    it(`should add products to location`, async () => {
      const { body } = await request(app.getHttpServer())
        .post('/product/addProducts')
        .send(productsBody);

      expect(HttpStatus.CREATED);
      expect(body).toStrictEqual(
        expect.arrayContaining([
          expect.objectContaining({
            product: {
              id: 45555,
              size: '2X',
            },
          }),
          expect.objectContaining({
            product: {
              id: 45554,
              size: '2XL',
            },
          }),
        ]),
      );
    });

    it(`it should throw if invalid rack or sectionNumber is provided`, async () => {
      const productsBody = {
        rack: 'CC',
        sectionNumber: 11,
        products: [
          {
            id: 'L45555 S2X',
          },
        ],
      };
      const { body } = await request(app.getHttpServer())
        .post('/product/addProducts')
        .send(productsBody)
        .expect(HttpStatus.BAD_REQUEST);

      expect(body.message).toBe(
        `There is no such location with rack = ${productsBody.rack} and section = ${productsBody.sectionNumber}`,
      );
    });
  });

  describe('DELETE /products/deleteProducts', () => {
    it(`should delete products from location`, async () => {
      const { status, body } = await request(app.getHttpServer())
        .delete('/product/deleteProducts')
        .send(productsBody);

      expect(status).toBe(HttpStatus.OK);
      expect(body).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            updated: expect.arrayContaining([]),
          }),
          expect.objectContaining({
            deleted: expect.arrayContaining([
              expect.objectContaining({
                id: 45555,
                size: '2X',
              }),
              expect.objectContaining({
                id: 45554,
                size: '2XL',
              }),
            ]),
          }),
        ]),
      );
    });

    it(`it should throw if invalid rack or sectionNumber is provided`, async () => {
      const productsBody = {
        rack: 'CC',
        sectionNumber: 11,
        products: [
          {
            id: 'L45555 S2X',
          },
        ],
      };
      const { body } = await request(app.getHttpServer())
        .delete('/product/deleteProducts')
        .send(productsBody)
        .expect(HttpStatus.BAD_REQUEST);

      expect(body.message).toBe(
        `There is no such location with rack = ${productsBody.rack} and section = ${productsBody.sectionNumber}`,
      );
    });
  });
});
